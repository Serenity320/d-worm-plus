#include <windows.h>
#include <time.h>
#include "resource.h"

#define INTERVAL 150

#define WIDTH 25
#define HEIGHT 25

#define UP 2
#define DOWN 4
#define LEFT 6
#define RIGHT 8

#define LEVEL 4
#define STAGE 6
#define ITEM 10
#define MONSTER 5

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);
HINSTANCE g_hInst;
LPSTR lpszClass="D-War Plus";

void init();
void init_start();
void init_setting();
void move();
void check();
void item_add();
void item_timer();
void mon_add();
void mon_timer();
void point();

/* Queue Linked List */
void put(int x, int y, int state);
void get();
void clear();
void clash();

typedef struct NODE
{
	int data_x;
	int data_y;
	int state;
	struct NODE* prev;
	struct NODE* next;
}NODE;

typedef struct Game
{
	int map[WIDTH][HEIGHT];
	int level;
	int stage;
	int state;
	int item_sign;
	int save_sign;
	int timer;
	int interval;
	BOOL key_check;
	BOOL move_check;
	int length_check;
	BOOL save_check;
	int length_temp;
	int point_temp;
	int tot_point_temp;
	int tot_point_count;
	char rnd_temp[10];
	int menu;
	char cheat[10];
	BOOL cheat_mode;
}Game;

typedef struct Worm
{
	int x;
	int y;
	int state;
	int length;
	int save;
	int point;
	int tot_point;
}Worm;

typedef struct Item
{
	int x;
	int y;
	int state;
	int timer;
	int pro;
	int sec;
	int turn;
	float effect;
	int rnd;
}Item;

typedef struct Monster
{
	int x[40];
	int y[40];
	int state;
	int timer;
	int stage;
	int num;
	int length;
	int sec;
	int rnd;
	BOOL save_check[40];
}Monster;

NODE *head, *tail;

Game game;
Worm worm;
Item item[30];
Monster mon[35];

HDC hdc, MemDC, BitDC;
HBITMAP MemBit;
HBITMAP BitStart;
HBITMAP BitLevel;
HBITMAP BitClear;
HBITMAP BitOver;
HBITMAP BitBack;
HBITMAP BitMenu;
HBITMAP BitWall;
HBITMAP BitWorm;
HBITMAP BitItem;
HBITMAP BitMon26;
HBITMAP BitMon27;
HBITMAP BitMon28;
HBITMAP BitMon29;
HBITMAP BitMon30;

char str[256];

int i, j, k, l, m;
int map_wall[LEVEL]={25, 6, 4, 2};
int map_size[STAGE]={1, 1, 1, 1, 1, 1};
int item_sec[ITEM]={10, 10, 5, 5, 5, 5, 5, 5, 5, 5};
int item_pro[ITEM]={10, 100, 1000, 1000 , 1000, 1000, 1000, 1000, 1000, 1000};
//int item_pro[ITEM]={1, 1, 1, 1 , 1, 1, 1, 1, 1, 1};
int mon_stage[MONSTER]={2, 3, 4, 5, 6};
int mon_num[LEVEL][MONSTER]={{6, 2, 2, 2, 2}, {10, 4, 4, 4, 4}, {20, 6, 6, 6, 6}, {20, 6, 6, 6, 6}};
int mon_size[MONSTER]={1, 3, 5, 7, 9};
int mon_sec[MONSTER]={3, 5, 5, 5, 5};

int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance
					 ,LPSTR lpszCmdParam,int nCmdShow)
{
	HWND hWnd;
	MSG Message;
	WNDCLASS WndClass;
	g_hInst=hInstance;
	
	WndClass.cbClsExtra=0;
	WndClass.cbWndExtra=0;
	WndClass.hbrBackground=(HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	WndClass.hIcon=LoadIcon(g_hInst,MAKEINTRESOURCE(IDI_ICON));
	WndClass.hInstance=hInstance;
	WndClass.lpfnWndProc=(WNDPROC)WndProc;
	WndClass.lpszClassName=lpszClass;
	WndClass.lpszMenuName=NULL;
	WndClass.style=CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&WndClass);
	
	hWnd=CreateWindow(lpszClass,lpszClass,WS_CAPTION | WS_SYSMENU,
		100,100,(((WIDTH*20)+100)+6),(((HEIGHT*20)+100)+28),
		NULL,(HMENU)NULL,hInstance,NULL);
	ShowWindow(hWnd,nCmdShow);
	
	while(GetMessage(&Message,0,0,0)) {
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT iMessage,WPARAM wParam,LPARAM lParam)
{	
	PAINTSTRUCT ps;
	switch(iMessage) {
	case WM_CREATE:
		BitStart=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_START));
		BitLevel=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_LEVEL));
		BitClear=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_CLEAR));
		BitOver=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_OVER));
		BitBack=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_BACK));
		BitMenu=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MENU));
		BitWall=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_WALL));
		BitWorm=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_WORM));
		BitItem=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_ITEMS));
		BitMon26=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MON26));
		BitMon27=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MON27));
		BitMon28=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MON28));
		BitMon29=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MON29));
		BitMon30=LoadBitmap(g_hInst, MAKEINTRESOURCE(IDB_MON30));
		SetTimer(hWnd, 1, 100, NULL);
		SetTimer(hWnd, 2, 100, NULL);
		init();
		return 0;
	case WM_TIMER:
		switch(wParam)
		{
		case 1 :
			if(game.state==1)
			{
				item_add();
				item_timer();
				mon_add();
				mon_timer();
			}
			break;
		case 2 :
			if(game.state==1)
			{
				move();
				check();
				clash();
				KillTimer(hWnd, 2);
				SetTimer(hWnd, 2, int(game.interval*item[18].effect*item[19].effect), NULL);
			}
			point();
			break;
		}
		InvalidateRect(hWnd, NULL, FALSE);
		return 0;
		case WM_KEYDOWN:
			switch(wParam)
			{
			case '1' : // 레벨 증가
				if(game.cheat_mode)
				{
					if(game.level==(LEVEL-1))
					{
						game.level=0;
					}
					else
					{
						game.level++;
					}
					game.state=2;
					init_setting();
				}
				break;
			case '2' : // 스테이지 증가
				if(game.cheat_mode)
				{
					if(game.stage==STAGE)
					{
						game.stage=1;
					}
					else
					{
						game.stage++;
					}
					game.state=2;
					init_setting();
				}
				break;
			case '3' : // 길이 증가
				if(game.cheat_mode)
				{
					worm.length++;
					worm.point+=10;
					worm.tot_point+=10;
					game.length_check=4;
					put(worm.x, worm.y, worm.state);
				}
				break;
			case '4' : // 생명 증가
				if(game.cheat_mode)
				{
					worm.save=100;
				}
				break;
			case '5' : // 게임 속도 증가
				if(game.cheat_mode)
				{
					game.interval=50;
					game.length_check=5;
				}
				break;
			case '6' : // 게임 속도 감소
				if(game.cheat_mode)
				{
					game.interval=INTERVAL;
					game.length_check=5;
				}
				break;
			case VK_RETURN :
				switch(game.state)
				{
				case 0 :
					game.stage=1;
					game.state=2;
					init_setting();
					break;
				case 2 :
					game.state=1;
					game.save_sign=0;
					break;
				case 3 :
					game.state=1;
					game.save_sign=0;
					break;
				case 4 :
					switch(game.menu)
					{
					case 0 :
						game.level++;
						game.stage=1;
						game.state=2;
						init_setting();
						break;
					case 1 :
						init_start();
						break;
					}
					break;
					case 5 :
						init_start();
						break;
					case 6 :
						init_start();
						break;
				}
				break;
				case VK_UP :
					if(game.state==0 && game.level>0)
					{
						game.level--;
					}
					if(game.state==1 && game.key_check && worm.state!=DOWN)
					{
						worm.state=UP;
						game.key_check=FALSE;
					}
					if(game.state==4)
					{
						game.menu=0;
					}
					break;
				case VK_DOWN :
					if(game.state==0 && game.level<3)
					{
						game.level++;
					}
					if(game.state==1 && game.key_check && worm.state!=UP)
					{
						worm.state=DOWN;
						game.key_check=FALSE;
					}
					if(game.state==4)
					{
						game.menu=1;
					}
					break;
				case VK_LEFT :
					if(game.state==1 && game.key_check && worm.state!=RIGHT)
					{
						worm.state=LEFT;
						game.key_check=FALSE;
					}
					break;
				case VK_RIGHT :
					if(game.state==1 && game.key_check && worm.state!=LEFT)
					{
						worm.state=RIGHT;
						game.key_check=FALSE;
					}
					break;
				default :
					if(game.stage>0)
					{
						for(i=0; i<8; i++)
						{
							game.cheat[i]=game.cheat[i+1];
						}
						game.cheat[7]=wParam;
						if(strcmp(game.cheat, "SERENITY")==0)
						{
							if(!(game.cheat_mode))
							{
								game.cheat_mode=TRUE;
							}
							else
							{
								game.cheat_mode=FALSE;
							}
						}
					}
					break;
			}
			return 0;
			case WM_PAINT:
				hdc=BeginPaint(hWnd, &ps);

				MemDC=CreateCompatibleDC(hdc);
				BitDC=CreateCompatibleDC(hdc);
				MemBit=CreateCompatibleBitmap(hdc, ((WIDTH*20)+100), ((HEIGHT*20)+100));
				SelectObject(MemDC, MemBit);

				SetBkMode(MemDC, TRANSPARENT);

				switch(game.state)
				{
				case 0 :
					SelectObject(BitDC, BitStart);
					BitBlt(MemDC, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), BitDC, 0, 0, SRCCOPY);
					SelectObject(BitDC, BitMenu);
					BitBlt(MemDC, 230, (339+(game.level*43)), 20, 20, BitDC, 0, 0, SRCPAINT);
					BitBlt(MemDC, 230, (339+(game.level*43)), 20, 20, BitDC, 0, 20, SRCAND);
					break;
				case 4 :
					SelectObject(BitDC, BitLevel);
					BitBlt(MemDC, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), BitDC, 0, 0, SRCCOPY);
					SelectObject(BitDC, BitMenu);
					BitBlt(MemDC, 190, (339+(game.menu*43)), 20, 20, BitDC, 0, 0, SRCPAINT);
					BitBlt(MemDC, 190, (339+(game.menu*43)), 20, 20, BitDC, 0, 20, SRCAND);
					break;
				case 5 :
					SelectObject(BitDC, BitClear);
					BitBlt(MemDC, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), BitDC, 0, 0, SRCCOPY);
					break;
				case 6 :
					SelectObject(BitDC, BitOver);
					BitBlt(MemDC, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), BitDC, 0, 0, SRCCOPY);
					break;
				default :
					SelectObject(BitDC, BitBack);
					BitBlt(MemDC, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), BitDC, 0, 0, SRCCOPY);
					for(i=0; i<WIDTH; i++)
					{
						for(j=0; j<HEIGHT; j++)
						{
							if(game.map[i][j]==1)
							{
								SelectObject(BitDC, BitWall);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-1)*20), 0, SRCPAINT);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-1)*20), 20, SRCAND);
							}
							else if(game.map[i][j]>=2 && game.map[i][j]<=15)
							{
								SelectObject(BitDC, BitWorm);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-2)*20), 0, SRCPAINT);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-2)*20), 20, SRCAND);
							}
							else if(game.map[i][j]>=16 && game.map[i][j]<=25)
							{
								SelectObject(BitDC, BitItem);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-16)*20), 0, SRCPAINT);
								BitBlt(MemDC, (i*20), (j*20), 20, 20, BitDC, ((game.map[i][j]-16)*20), 20, SRCAND);
							}
						}
					}
					for(i=26; i<=30; i++)
					{
						for(j=0; j<int(mon[i].num*(item[20].effect*item[21].effect)); j++)
						{
							if(mon[i].state!=0 && !(mon[i].save_check[j]))
							{
								MoveToEx(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), (mon[i].x[j]*20)-(((mon[i].length/2)*20)), NULL);
								LineTo(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), (((mon[i].x[j]*20)+(((mon[i].length/2)*20)+20))-1));
								LineTo(MemDC, (((mon[i].y[j]*20)+(((mon[i].length/2)*20)+20)-1)), (((mon[i].x[j]*20)+(((mon[i].length/2)*20)+20))-1));
								LineTo(MemDC, (((mon[i].y[j]*20)+(((mon[i].length/2)*20)+20)-1)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)));
								LineTo(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)));
							}
							if(mon[i].state==2 && !(mon[i].save_check[j]))
							{
								switch(i)
								{
								case 26 :
									SelectObject(BitDC, BitMon26);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, 0, SRCPAINT);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, (mon[i].length*20), SRCAND);
									break;
								case 27 :
									SelectObject(BitDC, BitMon27);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, 0, SRCPAINT);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, (mon[i].length*20), SRCAND);
									break;
								case 28 :
									SelectObject(BitDC, BitMon28);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, 0, SRCPAINT);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, (mon[i].length*20), SRCAND);
									break;
								case 29 :
									SelectObject(BitDC, BitMon29);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, 0, SRCPAINT);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, (mon[i].length*20), SRCAND);
									break;
								case 30 :
									SelectObject(BitDC, BitMon30);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, 0, SRCPAINT);
									BitBlt(MemDC, ((mon[i].y[j]*20)-((mon[i].length/2)*20)), ((mon[i].x[j]*20)-((mon[i].length/2)*20)), (mon[i].length*20), (mon[i].length*20), BitDC, 0, (mon[i].length*20), SRCAND);
									break;
								}
							}
						}
					}
					switch(game.level)
					{
					case 0 :
						wsprintf(str,"Easy");
						break;
					case 1 :
						wsprintf(str,"Nomal");
						break;
					case 2 :
						wsprintf(str,"Hard");
						break;
					case 3 :
						wsprintf(str,"Hell");
						break;
					}
					TextOut(MemDC, ((WIDTH*20)+53), 43, str, strlen(str));
					wsprintf(str,"%d", game.stage);
					TextOut(MemDC, ((WIDTH*20)+67), 80, str, strlen(str));
					wsprintf(str,"%d", worm.length);
					TextOut(MemDC, ((WIDTH*20)+50), 123, str, strlen(str));
					wsprintf(str,"%d", worm.save);
					TextOut(MemDC, ((WIDTH*20)+52), 162, str, strlen(str));
					wsprintf(str,"%d", worm.point);
					TextOut(MemDC, ((WIDTH*20)+52), 202, str, strlen(str));
					wsprintf(str,"%d", worm.tot_point);
					TextOut(MemDC, ((WIDTH*20)+50-(game.tot_point_count*4)), 272, str, strlen(str));
					switch(game.state)
					{
					case 2 :
						wsprintf(str,"Enter를 누르면 게임이 시작 됩니다.", game.stage);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 3 :
						wsprintf(str,"Enter를 누르면 게임이 시작 됩니다.", game.stage);
						TextOut(MemDC, 50, (((WIDTH*20)+50)+3), str, strlen(str));
						switch(game.save_sign)
						{
						case 1 :
							wsprintf(str,"벽에 부딪혀 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, (((WIDTH*20)+50)-19), str, strlen(str));
							break;
						case 2 :
							wsprintf(str,"자살해서 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, (((WIDTH*20)+50)-19), str, strlen(str));
							break;
						case 26 :
							wsprintf(str,"개미와 싸우다 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, ((HEIGHT*20)+35), str, strlen(str));
							break;
						case 27 :
							wsprintf(str,"두더지에 먹혀 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, ((HEIGHT*20)+35), str, strlen(str));
							break;
						case 28 :
							wsprintf(str,"참새에 먹혀 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, ((HEIGHT*20)+35), str, strlen(str));
							break;
						case 29 :
							wsprintf(str,"늑대에 밟혀 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, ((HEIGHT*20)+35), str, strlen(str));
							break;
						case 30 :
							wsprintf(str,"탱크에 밟혀 죽었습니다. ( 생명 1 감소 )");
							TextOut(MemDC, 50, ((HEIGHT*20)+35), str, strlen(str));
							break;
						}
						break;
					}
					switch(game.item_sign)
					{
					case 16 :
						wsprintf(str,"%s먹이를 먹었습니다. ( 길이 1 증가, 점수 10 증가 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 17 :
						wsprintf(str,"%s폭탄에 맞았습니다. ( 길이 %d 감소, 점수 %d 감소 )", game.rnd_temp, game.length_temp, game.point_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 18 :
						wsprintf(str,"%s얼음를 먹었습니다. ( 10초 이동속도 0.5배 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 19 :
						wsprintf(str,"%s불꽃을 먹었습니다. ( 10초 이동속도 2배 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 20 :
						wsprintf(str,"%s네잎클로버를 먹었습니다. ( 2턴 몬스터 수 0.5배 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 21 :
						wsprintf(str,"%썩은네잎클로버를 먹었습니다. ( 2턴 몬스터 수 2배 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 22 :
						wsprintf(str,"%s동전를 주웠습니다. ( 점수 100 증가 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 23 :
						wsprintf(str,"%s하트를 먹었습니다. ( 생명 1 증가 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 24 :
						wsprintf(str,"%s독약을 먹었습니다. ( 생명 1 감소 )", game.rnd_temp);
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 25 :
						wsprintf(str,"점수 300점이 됬습니다. ( 점수 300 감소, 생명 1 증가 )");
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					case 26 :
						wsprintf(str,"개미와 싸워 이겼습니다. ( 길이 1 증가, 점수 50 증가 )");
						TextOut(MemDC, 50, (((WIDTH*20)+50)-8), str, strlen(str));
						break;
					}
}

if(game.cheat_mode)
{
	wsprintf(str,"치트활성화");
	TextOut(MemDC, 515, 580, str, strlen(str));
}

BitBlt(hdc, 0, 0, ((WIDTH*20)+100), ((HEIGHT*20)+100), MemDC, 0, 0, SRCCOPY);
DeleteDC(MemDC);
DeleteDC(BitDC);
DeleteObject(MemBit);

EndPaint(hWnd, &ps);
return 0;
					case WM_DESTROY:
						DeleteDC(hdc);
						DeleteDC(MemDC);
						DeleteDC(BitDC);
						DeleteObject(BitStart);
						DeleteObject(BitLevel);
						DeleteObject(BitClear);
						DeleteObject(BitOver);
						DeleteObject(BitBack);
						DeleteObject(BitMenu);
						DeleteObject(BitWall);
						DeleteObject(BitWorm);
						DeleteObject(BitItem);
						DeleteObject(BitMon26);
						DeleteObject(BitMon27);
						DeleteObject(BitMon28);
						DeleteObject(BitMon29);
						DeleteObject(BitMon30);
						KillTimer(hWnd, 1);
						KillTimer(hWnd, 2);
						PostQuitMessage(0);
						return 0;
	}
	return(DefWindowProc(hWnd,iMessage,wParam,lParam));
}

void init()
{
	/* Queue Linked List Init*/
	head=(NODE *)malloc(sizeof(NODE));
	tail=(NODE *)malloc(sizeof(NODE));
	head->prev=head;
	head->next=tail;
	tail->prev=head;
	tail->next=tail;
	
	srand((unsigned int)time(NULL));
	init_start();
	game.cheat_mode=FALSE;
}

void init_start()
{
	game.level=0;
	game.stage=0;
	game.state=0;
	game.save_sign=0;
	for(i=0; i<10; i++)
	{
		game.cheat[i]=NULL;
	}
	worm.save=3;
	worm.point=0;
	worm.tot_point=0;
}

void init_setting()
{
	for(i=0; i<WIDTH; i++)
	{
		for(j=0; j<HEIGHT; j++)
		{
			if(i<=(map_size[game.stage-1]-1) || i>=(WIDTH-map_size[game.stage-1]) || j<=(map_size[game.stage-1]-1) || j>=(HEIGHT-map_size[game.stage-1]) || (i%map_wall[game.level]==0 && j%map_wall[game.level]==0))
			{
				game.map[i][j]=1;
			}
			else
			{
				game.map[i][j]=0;
			}
		}
	}
	game.item_sign=0;
	game.timer=0;
	game.interval=INTERVAL;
	game.key_check=FALSE;
	game.move_check=FALSE;
	game.length_check=0;
	game.save_check=FALSE;
	game.length_temp=NULL;
	game.point_temp=NULL;
	game.tot_point_temp=NULL;
	game.tot_point_count=NULL;
	game.menu=0;
	strcpy(game.rnd_temp, "");
	worm.x=(map_size[game.stage-1]+2);
	worm.y=(map_size[game.stage-1]+4);
	worm.state=RIGHT;
	worm.length=3;
	for(i=16, j=0; i<=25; i++, j++)
	{
		item[i].x=NULL;
		item[i].y=NULL;
		item[i].state=0;
		item[i].timer=0;
		item[i].pro=item_pro[j];
		item[i].sec=item_sec[j];
		item[i].turn=0;
		item[i].effect=1;
		item[i].rnd=NULL;
	}
	for(i=26, j=0; i<=30; i++, j++)
	{
		for(k=0; k<(mon_num[game.level][j]*2); k++)
		{
			mon[i].x[k]=NULL;
			mon[i].y[k]=NULL;
			mon[i].save_check[k]=FALSE;
		}
		mon[i].state=0;
		mon[i].timer=0;
		mon[i].stage=mon_stage[j];
		mon[i].num=mon_num[game.level][j];
		mon[i].length=mon_size[j];
		mon[i].sec=mon_sec[j];
		mon[i].rnd=NULL;
	}
	clear();
	put((map_size[game.stage-1]+2), (map_size[game.stage-1]+2), worm.state);
	put((map_size[game.stage-1]+2), (map_size[game.stage-1]+3), worm.state);
	put((map_size[game.stage-1]+2), (map_size[game.stage-1]+4), worm.state);
}

void move()
{
	if(worm.state==UP)
	{
		worm.x--;
	}
	else if(worm.state==DOWN)
	{
		worm.x++;
	}
	else if(worm.state==LEFT)
	{
		worm.y--;
	}
	else if(worm.state==RIGHT)
	{
		worm.y++;
	}
}

void check()
{
	game.move_check=TRUE;
	
	/* 점수 Check */
	if(worm.point>=300)
	{
		game.item_sign=25;
		worm.save++;
		worm.point-=300;
	}
	
	/* 지렁이 길이 Check */
	if(game.length_check==0 && worm.length==5)
	{
		game.interval=(INTERVAL-10);
		game.length_check++;
	}
	else if(game.length_check==1 && worm.length==10)
	{
		game.interval=(INTERVAL-30);
		game.length_check++;
	}
	else if(game.length_check==2 && worm.length==15)
	{
		game.interval=(INTERVAL-50);
		game.length_check++;
	}
	else if(game.length_check==3 && worm.length==18)
	{
		game.interval=(INTERVAL-70);
		game.length_check++;
	}
	else if(game.length_check==4 && worm.length==20)
	{
		if(game.stage==STAGE)
		{
			if(game.level==(LEVEL-1))
			{
				game.state=5;
			}
			else
			{
				game.state=4;
			}
			init_setting();
		}
		else
		{
			game.stage++;
			game.state=2;
			game.move_check=FALSE;
			init_setting();
		}
	}
	
	/* 충돌, 아이템 Check*/
	if(game.save_check)
	{
		worm.save--;
		if(worm.save==0)
		{
			game.state=6;
			init_setting();
		}
		else
		{
			game.state=3;
			init_setting();
		}
	}
	switch(game.map[worm.y][worm.x])
	{
	case 1 :
		game.save_sign=1;
		game.save_check=TRUE;
		break;
	case 16 :
		strcpy(game.rnd_temp, "");
		game.item_sign=16;
		game.timer=0;
		game.move_check=FALSE;
		worm.length++;
		worm.point+=10;
		worm.tot_point+=10;
		item[16].state=0;
		put(worm.x, worm.y, worm.state);
		break;
	case 17 :
		strcpy(game.rnd_temp, "");
		game.item_sign=17;
		game.timer=0;
		item[17].state=0;
		switch(worm.length)
		{
		case 3 :
			game.length_temp=0;
			game.point_temp=10;
			worm.point-=10;
			worm.tot_point-=10;
			break;
		case 4 :
			game.length_temp=1;
			game.point_temp=20;
			worm.length-=1;
			worm.point-=20;
			worm.tot_point-=10;
			get();
			break;
		case 5 :
			game.length_temp=2;
			game.point_temp=30;
			worm.length-=2;
			worm.point-=30;
			worm.tot_point-=30;
			get();
			get();
			break;
		default :
			game.length_temp=3;
			game.point_temp=50;
			worm.length-=3;
			worm.point-=50;
			worm.tot_point-=50;
			get();
			get();
			get();
		}
		break;
		case 18 :
			strcpy(game.rnd_temp, "");
			game.item_sign=18;
			game.timer=0;
			item[18].turn=100;
			item[18].state=0;
			break;
		case 19 :
			strcpy(game.rnd_temp, "");
			game.item_sign=19;
			game.timer=0;
			item[19].turn=100;
			item[19].state=0;
			break;
		case 20 :
			strcpy(game.rnd_temp, "");
			game.item_sign=20;
			game.timer=0;
			item[20].turn=2;
			item[20].state=0;
			break;
		case 21 :
			strcpy(game.rnd_temp, "");
			game.item_sign=21;
			game.timer=0;
			item[21].turn=2;
			item[21].state=0;
			break;
		case 22 :
			strcpy(game.rnd_temp, "");
			game.item_sign=22;
			game.timer=0;
			worm.point+=100;
			worm.tot_point+=100;
			item[22].state=0;
			break;
		case 23 :
			strcpy(game.rnd_temp, "");
			game.item_sign=23;
			game.timer=0;
			worm.save++;
			item[23].state=0;
			break;
		case 24 :
			strcpy(game.rnd_temp, "");
			if(worm.save==1)
			{
				game.save_sign=24;
				game.save_check=TRUE;
			}
			else
			{
				game.item_sign=24;
				worm.save--;
			}
			game.timer=0;
			item[24].state=0;
			break;
		case 25 :
			strcpy(game.rnd_temp, "[랜덤] ");
			item[25].state=0;
			item[25].rnd=rand()%9+16;
			switch(item[25].rnd)
			{
			case 16 :
				game.item_sign=16;
				game.timer=0;
				game.move_check=FALSE;
				worm.length++;
				worm.point+=10;
				worm.tot_point+=10;
				put(worm.x, worm.y, worm.state);
				break;
			case 17 :
				game.item_sign=17;
				game.timer=0;
				switch(worm.length)
				{
				case 3 :
					game.length_temp=0;
					game.point_temp=10;
					worm.point-=10;
					worm.tot_point-=10;
					break;
				case 4 :
					game.length_temp=1;
					game.point_temp=20;
					worm.length-=1;
					worm.point-=20;
					worm.tot_point-=10;
					get();
					break;
				case 5 :
					game.length_temp=2;
					game.point_temp=30;
					worm.length-=2;
					worm.point-=30;
					worm.tot_point-=30;
					get();
					get();
					break;
				default :
					game.length_temp=3;
					game.point_temp=50;
					worm.length-=3;
					worm.point-=50;
					worm.tot_point-=50;
					get();
					get();
					get();
				}
				break;
				case 18 :
					game.item_sign=18;
					game.timer=0;
					item[18].turn=100;
					break;
				case 19 :
					game.item_sign=19;
					game.timer=0;
					item[19].turn=100;
					break;
				case 20 :
					game.item_sign=20;
					game.timer=0;
					item[20].turn=2;
					break;
				case 21 :
					game.item_sign=21;
					game.timer=0;
					item[21].turn=2;
					break;
				case 22 :
					game.item_sign=22;
					game.timer=0;
					worm.point+=100;
					worm.tot_point+=100;
					break;
				case 23 :
					game.item_sign=23;
					game.timer=0;
					worm.save++;
					break;
				case 24 :
					strcpy(game.rnd_temp, "");
					if(worm.save==1)
					{
						game.save_check=TRUE;
					}
					else
					{
						game.item_sign=24;
						worm.save--;
					}
					game.timer=0;
			}
			break;
			case 26 :
				mon[26].rnd=rand()%2;
				switch(mon[26].rnd)
				{
				case 0 :
					game.item_sign=26;
					game.timer=0;
					game.move_check=FALSE;
					worm.length++;
					worm.point+=50;
					worm.tot_point+=50;
					for(i=0; i<10; i++)
					{
						if(mon[26].x[i]==worm.x && mon[26].y[i]==worm.y)
						{
							mon[26].save_check[i]=TRUE;
							break;
						}
					}
					put(worm.x, worm.y, worm.state);
					break;
				case 1 :
					game.save_sign=26;
					game.save_check=TRUE;
					break;
				}
				break;
				case 27 :
					game.save_sign=27;
					game.save_check=TRUE;
					break;
				case 28 :
					game.save_sign=28;
					game.save_check=TRUE;
					break;
				case 29 :
					game.save_sign=29;
					game.save_check=TRUE;
					break;
				case 30 :
					game.save_sign=30;
					game.save_check=TRUE;
					break;
	}
	if(game.move_check)
	{
		get();
		put(worm.x, worm.y, worm.state);
	}
}

void item_add()
{
	for(i=16; i<=25; i++)
	{
		item[i].rnd=rand()%item[i].pro;
		if(item[i].rnd==(item[i].pro/2))
		{
			if(game.state==1 && item[i].state==0)
			{
				item[i].x=rand()%(WIDTH-2)+1;
				item[i].y=rand()%(HEIGHT-2)+1;
				if(game.map[item[i].y][item[i].x]==0)
				{
					game.map[item[i].y][item[i].x]=i;
					item[i].state=1;
					item[i].timer=0;
				}
			}
		}
	}
}

void item_timer()
{
	if(game.item_sign>=16)
	{
		game.timer++;
		if(game.timer==30)
		{
			game.item_sign=0;
			game.timer=0;
		}
	}
	for(i=16; i<=25; i++)
	{
		if(item[i].state==1)
		{
			item[i].timer++;
			if(item[i].timer==(item[i].sec*10))
			{
				game.map[item[i].y][item[i].x]=0;
				item[i].state=0;
			}
		}
	}
	if(item[18].turn>0)
	{
		item[18].turn--;
		item[18].effect=2;
	}
	else
	{
		item[18].effect=1;
	}
	if(item[19].turn>0)
	{
		item[19].turn--;
		item[19].effect=0.5;
	}
	else
	{
		item[19].effect=1;
	}
}

void mon_add()
{
	for(i=26; i<=30; i++)
	{
		if(game.stage>=mon[26].stage)
		{
			mon[26].stage=game.stage;
		}
		if(game.state==1 && mon[i].state==0 && mon[i].stage==game.stage)
		{
			if(item[20].turn>0)
			{
				item[20].turn--;
				item[20].effect=0.5;
			}
			else
			{
				item[20].effect=1;
			}
			if(item[21].turn>0)
			{
				item[21].turn--;
				item[21].effect=2;
			}
			else
			{
				item[21].effect=1;
			}
			for(j=0; j<int(mon[i].num*(item[20].effect*item[21].effect)); j++)
			{
				mon[i].x[j]=rand()%(((WIDTH-1)-mon[i].length)-((map_size[game.stage-1]*2)-1))+(((mon[i].length+1)/2)+map_size[game.stage-1]);
				mon[i].y[j]=rand()%(((HEIGHT-1)-mon[i].length)-((map_size[game.stage-1]*2)-1))+(((mon[i].length+1)/2)+map_size[game.stage-1]);
			}
			mon[i].state=1;
			mon[i].timer=0;
		}
	}
}

void mon_timer()
{
	for(i=26; i<=30; i++)
	{
		if(game.stage>=mon[i].stage && mon[i].state!=0 && mon[i].timer!=((mon[i].sec*100)+30))
		{
			mon[i].timer++;
			if(mon[i].timer==30)
			{
				mon[i].state=2;
			}
			else if(mon[i].timer==((mon[i].sec*10)+30))
			{
				mon[i].state=0;
			}
			for(j=0; j<int(mon[i].num*(item[20].effect*item[21].effect)); j++)
			{
				for(k=-(mon[i].length/2); k<=(mon[i].length/2); k++)
				{
					for(l=-(mon[i].length/2); l<=(mon[i].length/2); l++)
					{
						switch(mon[i].state)
						{
						case 2 :
							for(m=16; m<=25; m++)
							{
								if(game.map[mon[i].y[j]+k][mon[i].x[j]+l]==m)
								{
									if(item[i].state!=2)
									{
										item[m].state=0;
										item[m].timer=0;
									}
								}
							}
							if(game.map[mon[i].y[j]+k][mon[i].x[j]+l]==1)
							{
								game.map[mon[i].y[j]+k][mon[i].x[j]+l]=1;
							}
							else if(!(mon[i].save_check[j]))
							{
								game.map[mon[i].y[j]+k][mon[i].x[j]+l]=i;
							}
							break;
						case 0 :
							if(game.map[mon[i].y[j]+k][mon[i].x[j]+l]==i)
							{
								game.map[mon[i].y[j]+k][mon[i].x[j]+l]=0;
							}
							break;
						}
					}
				}
			}
		}
	}
}

void point()
{
	game.tot_point_count=0;
	game.tot_point_temp=worm.tot_point;
	do
	{
		game.tot_point_temp/=10;
		game.tot_point_count++;
	}while(game.tot_point_temp!=0);
}

/* Queue Linked List */
void put(int x, int y, int state)
{
	NODE* temp;
	temp=(NODE*)malloc(sizeof(NODE));
	temp->data_x=x;
	temp->data_y=y;
	temp->state=state;
	tail->prev->next=temp;
	temp->prev=tail->prev;
	temp->next=tail;
	tail->prev=temp;
	temp->next=tail;
	switch(tail->prev->state)
	{
	case UP :
		game.map[tail->prev->data_y][tail->prev->data_x]=2;
		if(tail->prev->prev->state==LEFT)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=13;
		}
		else if(tail->prev->prev->state==RIGHT)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=12;
		}
		else if(tail->prev->prev->state==UP)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=10;
		}
		break;
	case DOWN :
		game.map[tail->prev->data_y][tail->prev->data_x]=3;
		if(tail->prev->prev->state==LEFT)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=15;
		}
		else if(tail->prev->prev->state==RIGHT)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=14;
		}
		else if(tail->prev->prev->state==DOWN)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=10;
		}
		break;
	case LEFT :
		game.map[tail->prev->data_y][tail->prev->data_x]=4;
		if(tail->prev->prev->state==UP)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=14;
		}
		else if(tail->prev->prev->state==DOWN)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=12;
		}
		else if(tail->prev->prev->state==LEFT)
		{
			game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=11;
		}
		break;
	case RIGHT :
		game.map[tail->prev->data_y][tail->prev->data_x]=5;
		if(tail->prev->prev!=head)
		{
			if(tail->prev->prev->state==UP)
			{
				game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=15;
			}
			else if(tail->prev->prev->state==DOWN)
			{
				game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=13;
			}
			else if(tail->prev->prev->state=RIGHT)
			{
				game.map[tail->prev->prev->data_y][tail->prev->prev->data_x]=11;
			}
		}
		break;
	}
	switch(head->next->next->state)
	{
	case UP :
		game.map[head->next->data_y][head->next->data_x]=6;
		break;
	case DOWN :
		game.map[head->next->data_y][head->next->data_x]=7;
		break;
	case LEFT :
		game.map[head->next->data_y][head->next->data_x]=8;
		break;
	case RIGHT :
		game.map[head->next->data_y][head->next->data_x]=9;
		break;
	}
	game.key_check=TRUE;
}

void get()
{
	int x, y;
	NODE* temp;
	temp=head->next;
	x=temp->data_x;
	y=temp->data_y;
	head->next=temp->next;
	temp->next->prev=head;
	free(temp);
	game.map[y][x]=0;
}

void clear()
{
	NODE *temp, *clear;
	temp=head->next;
	while(temp!=tail)
	{
		clear=temp;
		temp=temp->next;
		free(clear);
	}
	head->next=tail;
	tail->prev=head;
}

void clash()
{
	int x, y;
	NODE *temp;
	x=tail->prev->data_x;
	y=tail->prev->data_y;
	temp=head->next;
	while(temp!=tail->prev)
	{
		if(x==temp->data_x && y==temp->data_y)
		{
			game.save_sign=2;
			game.save_check=TRUE;
		}
		temp=temp->next;
	}
}