//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Script.rc
//
#define IDB_START                       101
#define IDB_BACK                        102
#define IDB_LEVEL                       103
#define IDB_CLEAR                       104
#define IDB_OVER                        105
#define IDB_MENU                        106
#define IDB_WALL                        107
#define IDB_WORM                        108
#define IDB_ITEMS                       109
#define IDB_MON26                       110
#define IDB_MON27                       111
#define IDB_MON28                       112
#define IDB_MON29                       113
#define IDB_MON30                       114
#define IDI_ICON                        200

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
